# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""Tools to facilitate the renaming of discipline input and output variables."""

from __future__ import annotations

import logging
from collections import defaultdict
from typing import TYPE_CHECKING
from typing import NamedTuple

from pandas import DataFrame
from pandas import read_csv
from pandas import read_excel
from prettytable import PrettyTable

from gemseo.core.discipline.data_processor import NameMapping
from gemseo.utils.repr_html import REPR_HTML_WRAPPER

if TYPE_CHECKING:
    from collections.abc import Iterable
    from collections.abc import Mapping
    from pathlib import Path

    from typing_extensions import Self

    from gemseo.core.discipline.discipline import Discipline


LOGGER = logging.getLogger(__name__)


class VariableRenamer:
    """Renamer of discipline input and output variable names."""

    __translations: tuple[VariableTranslation, ...]
    """The translations of the discipline input and output variables."""

    __translators: Mapping[str, Mapping[str, str]]
    """The translators."""

    def __init__(self) -> None:  # noqa: D107
        self.__translations = ()
        self.__translators = defaultdict(dict)

    def __get_pretty_table(self) -> PrettyTable:
        """Return a tabular view.

        Returns:
            A tabular view of the variable renamer.
        """
        pretty_table = PrettyTable()
        pretty_table.field_names = [
            "Discipline name",
            "Variable name",
            "New variable name",
        ]
        for translation in self.__translations:
            pretty_table.add_row([
                translation.discipline_name,
                translation.variable_name,
                translation.new_variable_name,
            ])

        return pretty_table

    def __repr__(self) -> str:
        return str(self.__get_pretty_table())

    def _repr_html_(self) -> str:
        return REPR_HTML_WRAPPER.format(self.__get_pretty_table().get_html_string())

    @property
    def translations(self) -> tuple[VariableTranslation, ...]:
        """The translations of the discipline input and output variables."""
        return self.__translations

    @property
    def translators(self) -> Mapping[str, Mapping[str, str]]:
        """The translators.

        As ``{discipline_name: {variable_name, new_variable_name}}``.
        """
        return self.__translators

    @classmethod
    def from_translations(
        cls, *translations: VariableTranslation | tuple[str, str, str]
    ) -> VariableRenamer:
        """Create from translations.

        Args:
            *translations: The translations
                of the discipline input and output variables.
                If ``tuple``,
                formatted as ``(discipline_name, variable_name, new_variable_name)``.

        Returns:
            A renamer.
        """
        renamer = cls()
        for translation in translations:
            renamer.add_translation(translation)

        return renamer

    @classmethod
    def from_dictionary(
        cls, translations: Mapping[str, Mapping[str, str]]
    ) -> VariableRenamer:
        """Create from dictionaries.

        Args:
            translations: The translations of the discipline input and output variables
                as ``{discipline_name: {variable_name: new_variable_name}}``.

        Returns:
            A renamer.
        """
        renamer = cls()
        for (
            discipline_name,
            variable_names_to_new_variable_names,
        ) in translations.items():
            renamer.add_translations_by_discipline(
                discipline_name, variable_names_to_new_variable_names
            )

        return renamer

    @classmethod
    def from_spreadsheet(cls, file_path: str | Path) -> Self:
        """Create from a spreadsheet file.

        Structured as `discipline_name, variable_name, new_variable_name`.

        Args:
            file_path: The path to the spreadsheet file.

        Returns:
            A renamer.
        """
        return cls.__from_dataframe(read_excel(file_path, header=None))

    @classmethod
    def from_csv(cls, file_path: str | Path, sep: str = ",") -> Self:
        """Create from a CSV file.

        Structured as `discipline_name, variable_name, new_variable_name`.

        Args:
            file_path: The path to the CSV file.
            sep: The separator character.

        Returns:
            A renamer.
        """
        return cls.__from_dataframe(read_csv(file_path, sep=sep, header=None))

    @classmethod
    def __from_dataframe(cls, dataframe: DataFrame) -> Self:
        """Create from a pandas dataframe.

        Args:
            dataframe: A pandas dataframe.

        Returns:
            A renamer.
        """
        translations = [
            VariableTranslation(
                discipline_name=discipline_name,
                variable_name=variable_name,
                new_variable_name=new_variable_name,
            )
            for (
                discipline_name,
                variable_name,
                new_variable_name,
            ) in dataframe.to_numpy()
        ]
        return cls.from_translations(*translations)

    def add_translation(
        self, translation: VariableTranslation | tuple[str, str, str]
    ) -> None:
        """Add a translation.

        Args:
            translation: A variable translation.
                If tuple,
                formatted as ``(discipline_name, variable_name, new_variable_name)``.

        Raises:
            ValueError: When a variable has already been renamed.
        """
        if not isinstance(translation, VariableTranslation):
            translation = VariableTranslation(
                discipline_name=translation[0],
                variable_name=translation[1],
                new_variable_name=translation[2],
            )

        self.__translations = (*self.__translations, translation)
        translator = self.__translators[translation.discipline_name]
        new_variable_name = translator.get(translation.variable_name)
        if new_variable_name is not None:
            msg = (
                f"In discipline {translation.discipline_name!r}, "
                f"the variable {translation.variable_name!r} cannot be renamed "
                f"to {translation.new_variable_name!r} "
                f"because it has already been renamed to {new_variable_name!r}."
            )
            if new_variable_name == translation.new_variable_name:
                LOGGER.warning(msg)
            else:
                raise ValueError(msg)

        translator[translation.variable_name] = translation.new_variable_name

    def add_translations_by_discipline(
        self,
        discipline_name: str,
        variable_names_to_new_variable_names: Mapping[str, str],
    ) -> None:
        """Add one or more translations for a given discipline.

        Args:
            discipline_name: The name of the discipline.
            variable_names_to_new_variable_names: The new variable names
                bound to the old variable names.
        """
        for (
            variable_name,
            new_variable_name,
        ) in variable_names_to_new_variable_names.items():
            self.add_translation(
                VariableTranslation(
                    discipline_name=discipline_name,
                    variable_name=variable_name,
                    new_variable_name=new_variable_name,
                )
            )

    def add_translations_by_variable(
        self,
        new_variable_name: str,
        discipline_names_to_variable_names: Mapping[str, str],
    ) -> None:
        """Add one or more translations for a same variable.

        Args:
            new_variable_name: The new name of the variable
                to rename discipline variables.
            discipline_names_to_variable_names: The variable names
                bound to the discipline names.
        """
        for (
            discipline_name,
            variable_name,
        ) in discipline_names_to_variable_names.items():
            self.add_translation(
                VariableTranslation(
                    discipline_name=discipline_name,
                    variable_name=variable_name,
                    new_variable_name=new_variable_name,
                )
            )


class VariableTranslation(NamedTuple):
    """The translation of a discipline input or output variable."""

    discipline_name: str
    """The name of the discipline."""

    variable_name: str
    """The name of the variable."""

    new_variable_name: str
    """The new name of the variable."""

    def __repr__(self) -> str:
        return (
            f"{self.discipline_name!r}.{self.variable_name!r}"
            f"={self.new_variable_name!r}"
        )


def rename_discipline_variables(
    disciplines: Iterable[Discipline], translators: Mapping[str, Mapping[str, str]]
) -> None:
    """Rename input and output variables of disciplines.

    Args:
        disciplines: The disciplines.
        translators: The translators
            of the form ``{discipline_name: {variable_name: new_variable_name}}``.

    Raises:
        ValueError: when a translator uses a wrong ``variable_name``.
    """
    for discipline in disciplines:
        translator = translators.get(discipline_name := discipline.name)
        if translator is None:
            LOGGER.warning("The discipline '%s' has no translator.", discipline_name)
            continue

        grammars = [discipline.io.input_grammar, discipline.io.output_grammar]
        for variable_name, new_variable_name in translator.items():
            variable_name_does_not_exist = True
            for grammar in grammars:
                if variable_name in grammar:
                    variable_name_does_not_exist = False
                    grammar.rename_element(variable_name, new_variable_name)

            if variable_name_does_not_exist:
                msg = (
                    f"The discipline {discipline_name!r} "
                    f"has no variable {variable_name!r}."
                )
                raise ValueError(msg)

        discipline.io.data_processor = NameMapping({
            new_variable_name: variable_name
            for variable_name, new_variable_name in translator.items()
        })
